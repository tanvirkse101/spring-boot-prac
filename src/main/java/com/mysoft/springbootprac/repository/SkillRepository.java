package com.mysoft.springbootprac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mysoft.springbootprac.model.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {
}
