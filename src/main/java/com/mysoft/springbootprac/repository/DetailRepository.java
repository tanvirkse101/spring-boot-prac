package com.mysoft.springbootprac.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mysoft.springbootprac.model.Detail;

public interface DetailRepository extends JpaRepository<Detail, Long> {

}
