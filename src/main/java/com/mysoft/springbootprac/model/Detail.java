package com.mysoft.springbootprac.model;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "detail")
public class Detail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String firstname;
	private String lastname;
	@OneToOne(mappedBy = "detail")
	private User user;
}
